import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet } from "react-native";
import axios from "axios";
import { SearchBar } from "react-native-elements";
import useDebounce from "../hooks/useDebounce";

import api from "../api";
import { useAppDispatch } from "../store/redux/store";
import { movieSlice } from "../store/slices/movieSlice";
import { systemSlice } from "../store/slices/systemSlice";

const SearchHandler = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const [search, setSearch] = useState("");
  const debouncedSearch = useDebounce(search, 1000);

  useEffect(() => {
    if (debouncedSearch) {
      searchMovies();
    }
  }, [debouncedSearch]);

  const searchMovies = async () => {
    const body = {
      Title: search,
      take: 10,
      skip: 0,
    };
    axios
      .post(`${api.defaults.baseURL}/movies/search`, body)
      .then((response) => {
        dispatch(
          movieSlice.actions.setMovies({
            searchTerm: search,
            movies: response.data.movies,
          })
        );
        dispatch(
          systemSlice.actions.updateSystem({
            count: response.data.count,
          })
        );
      })
      .catch((e) => console.log(e));
  };

  return (
    <View>
      <SearchBar
        placeholder="Search..."
        value={search}
        onChangeText={(search) => setSearch(search)}
      />
    </View>
  );
};

export { SearchHandler };
