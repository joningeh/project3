import axios from "axios";

// You will need to set this to your ip address when running. This is the only variable that has to be changed.
export default axios.create({
  baseURL: "http://[THIS_IS_WHERE_YOUR_IP_GOES]:3001",
});
