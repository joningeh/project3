import { RouteProp } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack/lib/typescript/src/types";
import { Movie } from "../store/slices/movieSlice";

export type StackParamList = {
  Home: undefined;
  Detail: { movie: Movie };
  Reviews: { movieId: number };
};

export type DetailScreenNavigationProp = StackNavigationProp<
  StackParamList,
  "Detail"
>;
export type DetailScreenRouteProp = RouteProp<StackParamList, "Detail">;

export type ReviewScreenNavigationProp = StackNavigationProp<
  StackParamList,
  "Reviews"
>;
export type ReviewScreenRouteProp = RouteProp<StackParamList, "Reviews">;

export type DetailProps = {
  navigation: DetailScreenNavigationProp;
  route: DetailScreenRouteProp;
};
