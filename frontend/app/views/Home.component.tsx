import React, { useEffect, useState } from "react";
import { View, FlatList, StyleSheet, Picker, Text } from "react-native";
import axios from "axios";

import { AppState, useAppDispatch } from "../store/redux/store";
import { SearchHandler } from "../components/SearchHandler.component";
import { useSelector } from "react-redux";
import api from "../api";
import { Movie, movieSlice } from "../store/slices/movieSlice";
import { MovieListItem } from "./MovieListItem.component";
import { systemSlice } from "../store/slices/systemSlice";

const Home: React.FC = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const movies = useSelector((state: AppState) => state.movies.movies);
  const searchTerm = useSelector((state: AppState) => state.movies.searchTerm);
  const count = useSelector((state: AppState) => state.system.count);
  const [refreshing, setRefreshing] = useState(false);
  const [sortValue, setSortValue] = useState("ASC");
  const [filterValue, setFilterValue] = useState("");

  // As it stands, this simply clear the movie page, implemented in case anything gets bugged.
  // The intended use of this function doesn't really make sense in a movie DB
  const refreshMovies = () => {
    setRefreshing(true);
    dispatch(movieSlice.actions.clearMovies());
    setRefreshing(false);
  };

  const fetchMoreMovies = async () => {
    // Not fetch more movies than in DB
    if (movies.length >= count) {
      return;
    }

    // We create the endpoint based on whether a genre has been set or not, these are seperate in the back-end
    const endpoint =
      filterValue == ""
        ? `${api.defaults.baseURL}/movies/${searchTerm}/${
            movies.length + 10
          }/${sortValue}`
        : `${api.defaults.baseURL}/movies/${searchTerm}/${
            movies.length + 10
          }/${sortValue}/${filterValue}`;

    axios
      .get(endpoint)
      .then((response) => {
        response.data.movies.map((movie: Movie) => {
          if (!movies.includes(movie)) {
            dispatch(movieSlice.actions.addMovie(movie));
          }

          dispatch(systemSlice.actions.updateSystem(response.data.count));
        });
      })
      .catch((e) => console.log(e));
  };

  // We need to clear the list and fetch new ones when genre or sorting has been changed
  useEffect(() => {
    dispatch(movieSlice.actions.clearMovies());
    fetchMoreMovies();
  }, [filterValue, sortValue]);

  return (
    <View style={styles.container}>
      <SearchHandler />
      <View style={{ flexDirection: "row" }}>
        <Picker
          selectedValue={sortValue}
          style={{ height: 50, width: "50%" }}
          onValueChange={(itemValue, itemIndex) => setSortValue(itemValue)}
        >
          <Picker.Item label="Ascending A-Z" value="ASC" />
          <Picker.Item label="Descending Z-A" value="DESC" />
        </Picker>
        <Picker
          selectedValue={filterValue}
          style={{ height: 50, width: "50%" }}
          onValueChange={(itemValue, itemIndex) => setFilterValue(itemValue)}
        >
          <Picker.Item label="All" value="" />
          <Picker.Item label="Action" value="Action" />
          <Picker.Item label="Adventure" value="Adventure" />
          <Picker.Item label="Biography" value="Biography" />
          <Picker.Item label="Comedy" value="Comedy" />
          <Picker.Item label="Crime" value="Crime" />
          <Picker.Item label="Drama" value="Drama" />
          <Picker.Item label="Fantasy" value="Fantasy" />
          <Picker.Item label="History" value="History" />
          <Picker.Item label="Thriller" value="Thriller" />
        </Picker>
      </View>
      <View>
        <FlatList
          data={movies}
          renderItem={({ item }) => <MovieListItem {...item} />}
          keyExtractor={(item, index) =>
            Math.floor(Math.random() * 100).toString() +
            item.Column_1.toString()
          }
          ItemSeparatorComponent={() => <View></View>}
          refreshing={refreshing}
          onRefresh={refreshMovies}
          onMomentumScrollEnd={() => fetchMoreMovies()}
        />
      </View>
      <Text>You either haven't searched or there are no more movies.</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#eee",
    flex: 1,
  },
});

export default Home;
