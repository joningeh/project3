import React, { useState } from "react";
import {
  FlatList,
  View,
  TextInput,
  Button,
  Alert,
  StyleSheet,
} from "react-native";
import { useSelector } from "react-redux";
import { AppState, useAppDispatch } from "../store/redux/store";
import { ReviewListItem } from "./ReviewListItem.component";
import { ReviewScreenRouteProp } from "../types/navigation";
import { useRoute } from "@react-navigation/native";
import axios from "axios";
import api from "../api";
import { reviewSlice } from "../store/slices/reviewSlice";

const ReviewList = (): JSX.Element => {
  const route = useRoute<ReviewScreenRouteProp>();
  const reviews = useSelector((state: AppState) => state.reviews.reviews);
  const dispatch = useAppDispatch();
  const [reviewScore, setReviewScore] = useState(1);
  const [reviewInput, setReviewInput] = useState("");

  const { movieId } = route.params;

  // Might need this function in order to fetch more, should have pagination on reviews, need support in backend
  /*
  const fetchMoreReviews = async () => {
    axios
      .get(`${api.defaults.baseURL}/reviews/${movieId}`)
      .then((response) => {
        dispatch(reviewSlice.actions.setReviews({reviews: response.data}));
      })
      .catch((e) => console.log(e));
  };
  */

  const addReview = () => {
    const body = {
      Column_1: movieId,
      rating: reviewScore,
      review: reviewInput,
    };
    axios
      .post(`${api.defaults.baseURL}/review/new`, body)
      .then((response) => {
        console.log(response.data);
        dispatch(reviewSlice.actions.addReview(response.data));
      })
      .catch((e) => console.log(e));
  };

  const validateNumber = (text: string) => {
    if (!isNaN(Number(text))) {
      setReviewScore(Number(text));
    } else {
      Alert.alert(
        "Bad number input",
        "You need to input a number for the review, not text."
      );
    }
  };

  return (
    <View>
      <View style={styles.reviewWrapper}>
        <TextInput
          placeholder={"Score..."}
          value={reviewScore.toString()}
          underlineColorAndroid="transparent"
          keyboardType={"numeric"}
          onChangeText={(text) => validateNumber(text)}
          style={styles.numberInput}
        />
        <TextInput
          placeholder="Write your review..."
          value={reviewInput}
          onChangeText={(text) => setReviewInput(text)}
          style={styles.textInput}
        />
        <Button onPress={addReview} title="Submit Review" color="#000" />
      </View>

      <FlatList
        data={reviews}
        renderItem={({ item }) => <ReviewListItem {...item} />}
        keyExtractor={(item) => item.id.toString()}
        ItemSeparatorComponent={() => <View></View>}
        //onEndReached={fetchMoreReviews}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  reviewWrapper: {
    margin: 10,
    backgroundColor: "#fff",
  },
  numberInput: {
    height: 50,
    padding: 10,
    borderBottomWidth: 1,
  },
  textInput: {
    height: 200,
    padding: 10,
  },
});

export { ReviewList };
