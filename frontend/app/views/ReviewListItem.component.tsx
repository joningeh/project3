import React from "react";
import { ListItem } from "react-native-elements";
import { Review } from "../store/slices/reviewSlice";

const ReviewListItem = (review: Review): JSX.Element => {
  return (
    <ListItem>
      <ListItem.Content>
        <ListItem.Title>{review.rating}</ListItem.Title>
        <ListItem.Subtitle>{review.review}</ListItem.Subtitle>
      </ListItem.Content>
    </ListItem>
  );
};

export { ReviewListItem };
