import { useNavigation, useRoute } from "@react-navigation/native";
import React from "react";
import {
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  View,
} from "react-native";
import {
  DetailScreenRouteProp,
  ReviewScreenNavigationProp,
} from "../types/navigation";
import axios from "axios";
import api from "../api";
import { useAppDispatch } from "../store/redux/store";
import { reviewSlice } from "../store/slices/reviewSlice";

const MovieDetail = (): JSX.Element => {
  const route = useRoute<DetailScreenRouteProp>();
  const navigation = useNavigation<ReviewScreenNavigationProp>();
  const dispatch = useAppDispatch();

  // We get the movie from the navigation component when a user clicks on it
  const { movie } = route.params;

  // Fetch all the reviews for the movie
  const showReviews = async () => {
    axios
      .get(`${api.defaults.baseURL}/reviews/${movie.Column_1}`)
      .then((response) => {
        dispatch(reviewSlice.actions.setReviews({ reviews: response.data }));
        navigation.navigate("Reviews", { movieId: movie.Column_1 });
      })
      .catch((e) => console.log(e));
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={styles.title}>{movie.Title}</Text>
      <Image style={styles.image} source={{ uri: movie.Poster }} />
      <Text style={styles.text}>{movie.Plot}</Text>
      <Text style={styles.genre}>{movie.Genre}</Text>
      <View style={styles.infoWrapper}>
        <Text style={styles.info}>Metascore: {movie.Metascore}</Text>
        <Text style={styles.info}>IMDB score: {movie.imdbRating}</Text>
        <Text style={styles.info}>User rating: {movie.rating}</Text>
      </View>
      <View>
        <Text style={styles.info}>Director: {movie.Director}</Text>
        <Text style={styles.info}>Writer(s): {movie.Writer}</Text>
        <Text style={styles.info}>Actors: {movie.Actors}</Text>
      </View>
      <View style={styles.infoWrapper}>
        <Text style={styles.info}>{movie.Runtime}</Text>
        <Text style={styles.info}>Released: {movie.Year}</Text>
      </View>
      <TouchableOpacity onPress={() => showReviews()}>
        <Text style={styles.reviews}>Reviews</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexWrap: "wrap",
    alignContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#eee",
  },
  title: {
    fontSize: 20,
    padding: 10,
  },
  image: {
    width: 250,
    height: 300,
    padding: 10,
    margin: 10,
  },
  genre: {
    fontSize: 16,
    fontStyle: "italic",
  },
  text: {
    fontSize: 16,
    padding: 10,
  },
  infoWrapper: {
    flexDirection: "row",
    margin: 10,
  },
  info: {
    fontSize: 14,
    margin: 5,
    padding: 5,
  },
  reviews: {
    marginBottom: 20,
    fontSize: 18,
    color: "#0000ff",
    fontWeight: "bold",
    fontStyle: "italic",
    textDecorationLine: "underline",
  },
});

export { MovieDetail };
