import { useNavigation } from "@react-navigation/native";
import React from "react";
import { TouchableOpacity } from "react-native";
import { ListItem } from "react-native-elements";
import { Movie } from "../store/slices/movieSlice";
import { DetailScreenNavigationProp } from "../types/navigation";

const MovieListItem = (movie: Movie): JSX.Element => {
  const navigation = useNavigation<DetailScreenNavigationProp>();

  return (
    <TouchableOpacity
      onPress={() => navigation.navigate("Detail", { movie: movie })}
    >
      <ListItem>
        <ListItem.Content>
          <ListItem.Title>{movie.Title}</ListItem.Title>
          <ListItem.Subtitle>{movie.Genre}</ListItem.Subtitle>
          <ListItem.Subtitle>Metascore: {movie.Metascore}</ListItem.Subtitle>
        </ListItem.Content>
      </ListItem>
    </TouchableOpacity>
  );
};

export { MovieListItem };
