import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Base model for out system state
export interface System {
  count: number;
}

// Type of the state found in the store
type SystemState = {
  count: number;
};

// Our slices, this is essentially a combination of our state, reducers and actions found in our store.
export const systemSlice = createSlice({
  name: "system-slice",
  initialState: {
    count: 0,
  } as SystemState,
  reducers: {
    updateSystem: (state: SystemState, action: PayloadAction<SystemState>) => ({
      count: action.payload.count,
    }),
  },
});
