import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Base model for review
export interface Review {
  id: string;
  Column_1: number;
  rating: number;
  review: string;
}

// Type of the state found in the store
type ReviewState = {
  reviews: Array<Review>;
};

// Our slices, this is essentially a combination of our state, reducers and actions found in our store.
export const reviewSlice = createSlice({
  name: "review-slice",
  initialState: {
    reviews: [],
  } as ReviewState,
  reducers: {
    setReviews: (state: ReviewState, action: PayloadAction<ReviewState>) => ({
      reviews: action.payload.reviews,
    }),
    addReview: (state: ReviewState, action: PayloadAction<Review>) => ({
      reviews: [...state.reviews, action.payload],
    }),
  },
});
