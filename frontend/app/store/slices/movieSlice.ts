import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Base model for movie
export interface Movie {
  Column_1: number;
  Title: string;
  Year: string;
  Released: number;
  Runtime: string;
  Genre: string;
  Director: string;
  Writer: string;
  Actors: string;
  Plot: string;
  Poster: string;
  Metascore: number;
  imdbRating: number;
  rating: number;
}

// Type of the state found in the store
type MovieState = {
  searchTerm: string;
  movies: Array<Movie>;
};

// Our slices, this is essentially a combination of our state, reducers and actions found in our store.
export const movieSlice = createSlice({
  name: "movie-slice",
  initialState: {
    searchTerm: "",
    movies: [],
  } as MovieState,
  reducers: {
    setMovies: (state: MovieState, action: PayloadAction<MovieState>) => ({
      searchTerm: action.payload.searchTerm,
      movies: action.payload.movies,
    }),
    addMovie: (state: MovieState, action: PayloadAction<Movie>) => ({
      searchTerm: state.searchTerm,
      movies: [...state.movies, action.payload],
    }),
    clearMovies: (state: MovieState) => ({
      searchTerm: state.searchTerm,
      movies: [],
    }),
  },
});
