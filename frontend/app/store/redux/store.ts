import { configureStore } from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";
import { movieSlice } from "../slices/movieSlice";
import { reviewSlice } from "../slices/reviewSlice";
import { systemSlice } from "../slices/systemSlice";

/**
 * store contains the redux configuration and is a combination of all the states, actions and reducers made in slices.
 * Access to following:
 * @constructs movieSlice
 * @constructs reviewSlice
 * @constructs systemSlice
 */
export const store = configureStore({
  reducer: {
    movies: movieSlice.reducer,
    reviews: reviewSlice.reducer,
    system: systemSlice.reducer,
  },
});

// We need to create a type for typescript.
export type AppState = ReturnType<typeof store.getState>;

// We export the dispatching of our store in order to effectively use it in our components.
export const useAppDispatch = () => useDispatch<typeof store.dispatch>();
