import { StatusBar } from "expo-status-bar";
import React from "react";
import { Provider } from "react-redux";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import { store } from "./app/store/redux/store";
import { StackParamList } from "./app/types/navigation";
import Home from "./app/views/Home.component";
import { MovieDetail } from "./app/views/MovieDetail.component";
import { ReviewList } from "./app/views/ReviewList.component";

// We use the navigator component to allow for easy navigation inside of our application.
const Stack = createStackNavigator<StackParamList>();

const App: React.FC = (): JSX.Element => {
  return (
    <Provider store={store}>
      <StatusBar style="auto" />
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="Detail" component={MovieDetail} />
          <Stack.Screen name="Reviews" component={ReviewList} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
