### Project init

- Run `npm install` on root folder, backend folder and frontend folder.
- To access DB run `npm start` on backend folder.
- To launch React application run `npm start` on frontend folder.

IMPORTANT! The data is fetched from the back-end, if you're gonna test this solution on an android or iOS unit using expo. You have to make sure that the backend is fetched from your computer's IP and not the default localhost. Head into `frontend/app/api/index.ts` and configure the `baseURL` to be your own. If you don't know how to find your IP, type `ipconfig` (windows), `ifconfig | grep 'inet'` (mac) or `hostname -i` (debian). Example in Windows, the adapter called `Wireless LAN Adapter Wi-Fi` is most likely going to contain your correct IPv4 or IPv6 address. If you use an IPv6 address, you might have to wrap it with `[]`. Due to time constraints minimal effort went into improving the backend in this assignment as this was not specified in the requirements.

### Back-end

#### Setup

- Run `npm install` inside of `project3/backend`
- Run `npm start` to start the server. This will run on `localhost:3001`

#### Troubleshooting

The server allows a maximum of 5 concurrent connections as it's hosted on ElephantSQL's free-plan. In the case that you should not be able to connect, this is because 5 other student's are simultaneously running the server and therefore have an active connection to the database.

#### Summary from P3

- Backend is a Node.js server.
- Frameworks used: TypeOrm and Koa.
- DB running on ElephantSQL, which is a 'PostgreSQL as a Service'

#### Documentation found at

https://typeorm.io/#/
https://koajs.com/#
https://www.elephantsql.com/

Credit:
https://www.mfosullivan.com/rest-api-node-koa-postgresql/

## Front-end

The frontend was initialized using expo cli tools. It is a React Native application which can be easily tested on phones using the Expo app. You can read about expo at https://expo.io/tools.

#### Setup

IMPORTANT! The data is fetched from the back-end, if you're gonna test this solution on an android or iOS unit using expo. You have to make sure that the backend is fetched from your computer's IP and not the default localhost. Head into `frontend/app/api/index.ts` and configure the `baseURL` to be your own. If you don't know how to find your IP, type `ipconfig` (windows), `ifconfig | grep 'inet'` (mac) or `hostname -i` (debian). Due to time constraints minimal effort went into improving the backend in this assignment as this was not specified in the requirements.

- Run `npm install` inside of `projejct3/frontend`
- Run `npm start` to start the React application. This will run on `localhost:3000`

### 3rd party tools

- Expo

### The application

#### Search component

The application allows users to search the database for movies using React Native Elements' SearchBar component, fitted to our use. This will return any results matching the user's input. This is default sorted to ascending based on characters, but can be set to descending depending on what the user prefers. Sorting is handled in the back-end.

#### List view

Using the FlatList component together with redux, the user can load more results simply by scrolling to the bottom of the screen. This will not work if the application is tested in the browser, as the component is designed for functionality with mobile phones. This should not be an issue as React Native application are applications designed for andriod and iOS.

#### Detailed view

By pressing one of the queried items, a third party component React-Navigation made for React Native will swap the focus to a nested screen containing all the information about the movie. From here, the user can click on Reviews to go even deeper into the navigation component in order to fetch, read and write reviews related to the movie viewed. At any point in time the user can click the back button to go back to the previous screen, all the way back to "Home" where the searchbar and queried list will be displayed.

#### Sorting and filtration

The user can set parameters for sorting and filtration on a modal accessed through the Home screen before typing in the search. If these options are set after the search is performed, the user will have to search again in order to make a new query.

### Technology and testing

#### Typescript

As Typescript was mandatory, the expo project was initialized with Typescript. As a result all code in the frontend folder has been written in Typescript. A linter has also been configured for Typescript to ensure that my code was robust.

#### React Native and Expo

The React Native project was initialized using the Expo CLI tools.

Some of the third party libraries we have used:

- Redux, with React-Redux: state management.
- React Navigation: routing for the react native application
- React Native Elements: a cross platform UI toolkit. This has mainly been used for the searchbar as we wanted a guarantee that it would work properly on both Androis and iOS. We could have used more components, but considering that most of the logic we have used are based on components in the React-Native library without heavy styling rules and advanced logic, we assumed cross-platform support would not be an issue.

#### End-to-end testing

The application has been heavily tested during the development. This has been done using the Expo application for Android and connecting it to the Expo service running on my computer. we can only ensure that the application will run properly on a unit rurnning Android OS as we do not own an iphone or any Apple devices to test this on.

### ESLint

- npx eslint backend
- npx eslint frontend

More information about our linter can be found at https://eslint.org/docs/user-guide/getting-started.

#### Why we went for this solution

A linter is needed in order to ensure a robust codebase. Without it, we would be able to have poor code which would compile, but could have unforseen consequences during runtime. ESLint is configured for Typescript in this project.

### Development process

During the development of P4 both group members felt that we lacked knowledge regarding the functionality implemented by the other member. The reason for this was that during P3, one of us primarily worked on the back-end and redux, while the other handled the front-end. As a result of this, pair-programming was extensively used both while refactoring the back-end and redux, and developing the new React-Native application. Due to lack of time, and a lacking understanding of how to use redux with Typescript. The application we made in P3 had a lot of bugs and illogical handling of state. We only realized this after the fact. Refactoring the code allowed us to fix this, as well as reworking how we handled requests in the back-end. This resulted in the front-end developer getting a much better understanding of what was going on under the hood. Pair-programming on React Native also gave valuable insight to the back-end developer on how to make decent components in a React(/Native) application.
