//import type definitions from typeorm
import {
  Entity,
  PrimaryColumn,
  Column
} from 'typeorm';
import {IsInt, Min, Max} from 'class-validator';

@Entity('p3db')
export class Movie {
  @PrimaryColumn("int")
  @IsInt()
  Column_1: number;

  @Column("text")
  Title: string;

  @Column("int")
  @IsInt()
  Year: string;

  @Column("text")
  Released: string;

  @Column("text")
  Runtime: string;

  @Column("text")
  Genre: string;

  @Column("text")
  Director: string;

  @Column("text")
  Writer: string;

  @Column("text")
  Actors: string;

  @Column("text")
  Plot: string;

  @Column("text")
  Poster: string;

  @Column("text")
  Metascore: number;

  @Column("int")
  @IsInt()
  imdbRating: number;

  @Column("int")
  @IsInt()
  @Min(1)
  @Max(10)
  rating: number;
}
