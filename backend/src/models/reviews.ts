import {
  Entity,
  PrimaryGeneratedColumn,
  Column
} from 'typeorm';
import {IsInt} from 'class-validator';

@Entity('p3dbReviews')
export class Review {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column("int")
  @IsInt()
  Column_1: number;

  @Column("float")
  rating: number;

  @Column("text")
  review: string;
}
