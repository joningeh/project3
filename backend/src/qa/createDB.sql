CREATE TABLE "p3db" AS 
(SELECT "Column_1", "Title", "Year", "Released", "Runtime", "Genre", "Director", 
"Writer", "Actors", "Plot", "Poster", "Metascore", CAST("imdbRating" AS INT) 
FROM "public"."IMDB_Top250movies2_OMDB_Detailed")

ALTER TABLE "public"."p3db" ADD "rating" INT

CREATE TABLE "p3dbReviews" ("id" UUID DEFAULT uuid_generate_v4 (), "Column_1" INT, "rating" FLOAT, "review" TEXT)