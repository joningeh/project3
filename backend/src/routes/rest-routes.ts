import * as Router from "koa-router";
import controller = require("../controllers");

export const restRouter = new Router();

// Get request on 10 movies in DB
restRouter.get("/movies", controller.movie.getMovies);
// Get request on top ten movies in DB
restRouter.get("/movies/top10", controller.movie.getTopMovies);
// Get request for single movie through id
restRouter.get("/movies/:id", controller.movie.getMovieId);
// Post request for finding movies based on search
restRouter.post("/movies/search", controller.movie.searchMovie);
// Get request for finding movies based on search
restRouter.get("/movies/:title/:take/:order", controller.movie.getMovieByTitle)
// Get request for finding movies based on search and genre
restRouter.get("/movies/:title/:take/:order/:genre", controller.movie.getMovieByTitleAndGenre);
// Post request for finding movies based on category
restRouter.post("/movies/genre", controller.movie.searchMoviesByGenre);

// Post request for finding reviews based on movie id
restRouter.get("/reviews/:Column_1", controller.review.getReviews);
// Post request for adding a review to a movie
restRouter.post("/review/new", controller.review.addReview);
