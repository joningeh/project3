import { createConnection } from 'typeorm';

import { postgresTables } from './postgres-tables';

export const postgresDB = async () => {
  return await createConnection({
    type: 'postgres',
    host: 'hattie.db.elephantsql.com',
    port: 5432,
    username: 'kfirtwpu',
    password: 'GLFrcuCse94BasFzLgEVHz6HccC-NtBf',
    database: 'kfirtwpu',
    ssl: true,
    entities: postgresTables,
    logging: ['query', 'error'],
    synchronize: false,
  }).then((connection) => {
    console.log('Database connection established');
  });
};
