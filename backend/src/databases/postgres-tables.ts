/*
Import our models and create array with list of tables used when connecting to DB.
*/
import { Movie } from '../models/movies';
import { Review } from '../models/reviews';

export const postgresTables = [Movie, Review];
