import { Context } from "koa";
import { getManager, Repository, Like } from "typeorm";

import { Movie } from "../models/movies";

const ERROR_MSG1 =
  "Movie does not exist in the database, consider adding it for future users looking for the same movie.";
const ERROR_MSG2 =
  "Conflicting movie name. Consider adding identifier if distinct movies have identical names.";
const ERROR_MSG3 =
  "Somethink awful has happened because the database must have gone down as it is supposed to contain movies and this code works...";

/**
 * MovieController contains all the logic surrounding querying the database and performing operations on the results from the database regarding the Movie model.
 */
export default class MovieController {
  public static async getMovies(ctx: Context) {
    const movieRepository: Repository<Movie> = getManager().getRepository(
      Movie
    );

    const movies: Movie[] = await movieRepository.find({
      take: ctx.header.take,
      skip: ctx.header.skip,
    });

    ctx.status = 200;
    ctx.body = movies;
  }

  public static async getTopMovies(ctx: Context) {
    const movieRepository: Repository<Movie> = getManager().getRepository(
      Movie
    );

    const movies: Movie[] = await movieRepository.find({
      order: {
        Metascore: "ASC",
      },
      take: ctx.header.take,
      skip: ctx.header.skip,
    });

    if (movies) {
      ctx.status = 200;
      ctx.body = movies;
    } else {
      ctx.status = 400;
      ctx.body = ERROR_MSG3;
    }
  }

  public static async getMovieId(ctx: Context) {
    const movieRepository: Repository<Movie> = getManager().getRepository(
      Movie
    );

    const movie: Movie = await movieRepository.findOne(ctx.params.Column_1);

    if (movie) {
      ctx.status = 200;
      ctx.body = movie;
    } else {
      ctx.status = 400;
      ctx.body = ERROR_MSG1;
    }
  }

  // TODO: replace this with TypeORM query instead of having it as a post with return
  // Template for future searched as parameters can just be added to the where clause
  // pagination can be ensured by sending in a paremeter for take and skip value
  // e.g. take: ctx.request.body.name.take || 10
  //      skip: ctx.request.body.name.skip || 0
  public static async searchMovie(ctx: Context) {
    const movieRepository: Repository<Movie> = getManager().getRepository(
      Movie
    );

    const [movies, count]: any = await movieRepository.findAndCount({
      where: `"Title" ILIKE '%${ctx.request.body.Title}%'`,
      // order has to be either DESC or ASC
      order: { Title: ctx.request.body.order || "ASC" },
      take: ctx.request.body.take || 10,
      skip: ctx.request.body.skip || 0,
    });

    if (movies) {
      ctx.status = 200;
      ctx.body = { movies: movies, count: count };
    } else {
      ctx.status = 400;
      ctx.body = ERROR_MSG1;
    }
  }

  public static async getMovieByTitle(ctx: Context) {
    const movieRepository: Repository<Movie> = getManager().getRepository(
      Movie
    );
    const [movies, count]: [
      Movie[],
      number
    ] = await movieRepository.findAndCount({
      where: `"Title" ILIKE '%${ctx.params.title}%'`,
      // order has to be either DESC or ASC
      order: { Title: ctx.params.order || "ASC" },
      take: ctx.params.take,
      skip: Math.max(ctx.params.take - 10, 0),
    });

    if (movies) {
      ctx.status = 200;
      ctx.body = { movies: movies, count: count };
    } else {
      ctx.status = 400;
      ctx.body = ERROR_MSG1;
    }
  }

  public static async getMovieByTitleAndGenre(ctx: Context) {
    const movieRepository: Repository<Movie> = getManager().getRepository(
      Movie
    );
    const [movies, count]: [
      Movie[],
      number
    ] = await movieRepository.findAndCount({
      where: `"Title" ILIKE '%${ctx.params.title}%' AND "Genre" ILIKE '%${ctx.params.genre}%'`,
      // order has to be either DESC or ASC
      order: { Title: ctx.params.order || "ASC" },
      take: ctx.params.take,
      skip: Math.max(ctx.params.take - 10, 0),
    });

    if (movies) {
      ctx.status = 200;
      ctx.body = { movies: movies, count: count };
    } else {
      ctx.status = 400;
      ctx.body = ERROR_MSG1;
    }
  }

  // This is supposed to be a post
  // Expected body has categories
  // Take and skip for pagination
  public static async searchMoviesByGenre(ctx: Context) {
    const movieRepository: Repository<Movie> = getManager().getRepository(
      Movie
    );

    const [movies, count]: [
      Movie[],
      number
    ] = await movieRepository.findAndCount({
      where: `"Genre" ILIKE '%${ctx.request.body.Genre}%'`,
      // order has to be either DESC or ASC
      order: {
        Title: ctx.request.body.order,
      },
      take: ctx.request.body.take || 10,
      skip: ctx.request.body.skip || 0,
    });

    if (movies) {
      ctx.status = 200;
      ctx.body = movies;
    } else {
      ctx.status = 400;
      ctx.body = ERROR_MSG2;
    }
  }

  /*
  public static async updateMovie (ctx: Context) {
    const movieRepository: Repository<Movie> = getManager().getRepository(Movie);

    const movieToBeUpdated: Movie = await movieRepository.findOne(ctx.params.id);

    if (!movieToBeUpdated) {
      ctx.status = 400;
      ctx.body = ERROR_MSG1;
    }

    if (ctx.request.body.name) {movieToBeUpdated.name = ctx.request.body.name;}
    if (ctx.request.body.category) {movieToBeUpdated.category = ctx.request.body.category;}
    if (ctx.request.body.release) {movieToBeUpdated.release = ctx.request.body.release;}
    if (ctx.request.body.details) {movieToBeUpdated.details = ctx.request.body.details;}

    // Validation of posted data
    const errors: ValidationError[] = await validate(movieToBeUpdated);
    if (errors.length > 0 ) {
      ctx.status = 400;
      ctx.body = errors;
    } 
    // TODO: unit test to verify if this is redundant
    else if ( !await movieRepository.findOne(movieToBeUpdated.id)) {
      ctx.status = 400;
      ctx.body = ERROR_MSG1;
    }
    // If a different movie in the DB has the identical name, throw error
    else if (await movieRepository.findOne({ id: Not(Equal(movieToBeUpdated.id)), name: movieToBeUpdated.name})) {
      ctx.status = 400;
      ctx.body = ERROR_MSG2;
    } else {
      const movie = await movieRepository.save(movieToBeUpdated);
      ctx.status = 201;
      ctx.body = movie;
    }
  }

  public static async deleteMovie (ctx: Context) {
    const movieRepository: Repository<Movie> = getManager().getRepository(Movie);

    const movieToRemove: Movie = await movieRepository.findOne(ctx.params.id);

    if (!movieToRemove) {
      ctx.status = 400;
      ctx.body = ERROR_MSG1;
    } else {
      await movieRepository.remove(movieToRemove);
      ctx.status = 204;
    }
  }
*/
}
