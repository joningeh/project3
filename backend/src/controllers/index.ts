// Export all controllers from this file
export { default as movie } from './movie';
export { default as review } from './review';