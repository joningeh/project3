import { validate, ValidationError } from "class-validator";
import { Context } from "koa";
import { Movie } from "../models/movies";
import { getManager, Repository, Equal } from "typeorm";

import { Review } from "../models/reviews";

/**
 * ReviewController contains all the logic surrounding querying the database and performing operations on the results from the database regarding the Review model..
 */
export default class ReviewController {
  public static async getReviews(ctx: Context) {
    const reviewRepository: Repository<Review> = getManager().getRepository(
      Review
    );

    const reviews: Review[] = await reviewRepository.find({
      where: {
        Column_1: Equal(ctx.params.Column_1),
      },
    });

    ctx.status = 200;
    ctx.body = reviews;
  }

  public static async addReview(ctx: Context) {
    const reviewRepository: Repository<Review> = getManager().getRepository(
      Review
    );

    const reviewToAdd: Review = new Review();

    reviewToAdd.Column_1 = ctx.request.body.Column_1;
    reviewToAdd.rating = ctx.request.body.rating;
    reviewToAdd.review = ctx.request.body.review;

    const errors: ValidationError[] = await validate(reviewToAdd, {
      skipMissingProperties: true,
    });
    if (errors.length > 0) {
      ctx.status = 400;
      ctx.body = errors;
    } else {
      const review = await reviewRepository.save(reviewToAdd);

      // Below is the logic regarding the updating of a specific movies user score based on the newly added review.
      const movieRepository: Repository<Movie> = getManager().getRepository(
        Movie
      );
      const movieToUpdate: Movie = await movieRepository.findOne(
        review.Column_1
      );
      if (movieToUpdate) {
        const currentScore = movieToUpdate.rating || 0;
        const reviews: Review[] = await reviewRepository.find({
          where: {
            Column_1: Equal(ctx.request.body.Column_1),
          },
        });
        const reviewAmount = reviews.length;
        const updatedScore = Math.floor(
          (currentScore * (reviewAmount - 1) + review.rating) / reviewAmount
        );
        movieToUpdate.rating = updatedScore;
        movieRepository.save(movieToUpdate);
      }

      ctx.status = 200;
      ctx.body = review;
    }
  }
}
