import * as bodyParser from 'koa-bodyparser';

import { postgresDB } from './databases/postgres-db';
import { restRouter } from './routes/rest-routes';

const app = require('./app');
const cors = require('koa2-cors');
app.use(cors());

const bootstrap = async () => {

  await postgresDB().catch((e) => {
    console.log(e);
  });

  app.use(bodyParser());

  app.use(restRouter.routes(), restRouter.allowedMethods());

  //Tell the app to listen on port 3001
  app.listen(3001);
};

bootstrap();
