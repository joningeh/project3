# Initialization
- Build using npm v6.14.6 and node.js v12.18.3. Can't guarantee support beyond this version.
- First time, run `npm install`
- To start, run `npm start`
- Application is configured to listen on `port 3001`, this can be configured in app.js

### Routes
Information about handling of URL requests.

### Controllers
Code used to define the logic surrounding requests. Essentially the validation and domain logic of our REST API.

### Models
Entities used in the application. What to create, store and retrieve from the DB.

### Databases
Used to initialise a connection the the DB.

### QA
Testing and quality assurance of the application and sample data.

#### app.ts
Creates application instance

#### server.ts
Logic related to packages and functionality needed for the server.
Database connection and server routes.